
/* imc table */
create table if not exists imc (
  id int auto_increment not null,
  height varchar(256) unique not null, 
  weight varchar(256), 
  primary key(id)
);