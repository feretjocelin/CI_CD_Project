import { IImcCreate } from "../../../types/tables/imc/IImc";

//TODO: taille en cm
export function imcCalculator(imc: IImcCreate) {
    let imcCalc = imc.weight / (imc.height * imc.height)
    return imcCalc;
}