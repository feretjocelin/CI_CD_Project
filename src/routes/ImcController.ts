import { Body, Delete, Get, Path, Post, Put, Query, Route } from 'tsoa';
import { imcCalculator } from '../classes/Business/ImcCalculator/ImcCalculator';
import { Crud } from '../classes/Crud';
import { ICreateResponseImc } from '../types/api/ICreateResponse';
import { IIndexResponse } from '../types/api/IIndexQuery';
import { IUpdateResponse } from '../types/api/IUpdateResponse';
import { IImc, IImcCreate, IImcUpdate } from '../types/tables/imc/IImc';

const READ_COLUMNS = ['id', 'height', 'weight'];

/**
 * Un utilisateur de la plateforme.
 */
@Route("/imc")
export class ImcController {

  /**
   * Récupérer toutes les IMC.
   */
  @Get()
  public async getImcs(
    /** La page (zéro-index) à récupérer */
    @Query() page?: number,
    /** Le nombre d'éléments à récupérer (max 50) */
    @Query() limit?: number,
  ): Promise<IIndexResponse<IImc>> {
    return Crud.Index<IImc>({ page, limit }, 'imc', READ_COLUMNS);
  }

  @Post()
  public async calcImc(
    @Body() body: IImcCreate,
  ): Promise<ICreateResponseImc> {
    Crud.Create(body, 'imc');
    return {
      imc: imcCalculator(body)
    }
  }


}
