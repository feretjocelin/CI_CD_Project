export interface ICreateResponse {
  id: number;
}

export interface ICreateResponseImc {
  imc: number;
}