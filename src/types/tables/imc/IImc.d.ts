/**
 * Une IMC à calculer.
 */
export interface IImc {
  /** ID Unique */
  id: number;
  /** Taille */
  height: number;
  /** Poids */
  weight: number;
}

export type IImcCreate = Omit<IImc, 'id'>;
export type IImcUpdate = Partial<IImcCreate>;
export type IImcRO = Readonly<IImc>;