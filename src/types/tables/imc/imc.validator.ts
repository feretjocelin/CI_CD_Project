import Ajv, { JSONSchemaType } from 'ajv';
import { IImcCreate, IImcUpdate } from './IImc';



const ImcCreateSchema: JSONSchemaType<IImcCreate> = {
  type: "object",
  properties: {
    height: { type: 'number', nullable: false },
    weight: { type: 'number', nullable: false }
  },
  required: [],
  additionalProperties: false,
};


const ImcUpdateSchema: JSONSchemaType<IImcUpdate> = {
  type: "object",
  properties: {
    height: { type: 'number', nullable: true },
    weight: { type: 'number', nullable: true }
  },
  additionalProperties: false,
};

const ajv = new Ajv();
export const ImcCreateValidator = ajv.compile(ImcCreateSchema);
export const ImcUpdateValidator = ajv.compile(ImcUpdateSchema);