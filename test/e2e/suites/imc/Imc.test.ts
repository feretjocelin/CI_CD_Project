import axios from 'axios';
import chai, { expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { describe } from 'mocha';
import { DB } from '../../../../src/classes/DB';
import { ImcController } from '../../../../src/routes/ImcController';
import { RootDB } from '../../../utility/RootDb';
import { TestServer } from '../../../utility/TestServer';

chai.use(chaiAsPromised);

describe("Imc calculating", function () {

    before(async function () {
        // Vider la base de données de test
        await RootDB.Reset();
        // Lancer le serveur
        await TestServer.Start();
    });

    after(async function () {
        // Forcer la fermeture de la base de données
        await DB.Close();
        // Arreter le serveur
        await TestServer.Stop();
    });


    it("Calculate an Imc", async function () {
        const result = await axios.post(process.env.API_HOST + '/imc',
            {
                height: 1.90,
                weight: 98
            });
        chai.expect(result.status).to.equal(200);
        chai.expect(result.data.imc).to.equal(27.146814404432135);


    });

    it("Retrieve all weight and height in database", async function () {
        const result = await axios.get(process.env.API_HOST + '/imc');
        chai.expect(result.status).to.equal(200);
        chai.expect(result.data.rows[0].id).to.equal(1);
    });


});
