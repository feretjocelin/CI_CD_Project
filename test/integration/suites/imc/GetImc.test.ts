import chai, { expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { describe } from 'mocha';
import { DB } from '../../../../src/classes/DB';
import { ImcController } from '../../../../src/routes/ImcController';
import { RootDB } from '../../../utility/RootDb';
chai.use(chaiAsPromised);

describe("Imc calculating", function () {

  before(async function () {
    // Vider la base de données de test
    await RootDB.Reset();
  });

  after(async function () {
    // Forcer la fermeture de la base de données
    await DB.Close();
  });

  it("Calculate an Imc", async function () {
    const imc = new ImcController();
    const result = await imc.calcImc({
      height: 1.90,
      weight: 98
    });

    expect(result.imc).to.equal(27.146814404432135);
  });

  it("Retrieve all weight and height in database", async function () {
    const imc = new ImcController();
    const result = await imc.getImcs();
    expect(result.rows[0].id).to.equal(1);
  });


});
