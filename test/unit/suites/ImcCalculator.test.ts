import { expect } from 'chai';
import { describe } from 'mocha';
import { imcCalculator } from '../../../src/classes/Business/ImcCalculator/ImcCalculator';
import { IImcCreate } from '../../../src/types/tables/imc/IImc';

describe("AdView", function () {

    it("Should return an Imc", function () {
        let imc: IImcCreate = {
            height: 1.92,
            weight: 112
        }
        expect(imcCalculator(imc)).to.equal(30.381944444444446)
    })
})